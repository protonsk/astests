package ru.proto.clicker.ui;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;



public class ASTestsAPI extends BaseUITest  {
    String promoCode = "123";
    String fName = "qwe";
    String lName = "qwe";
    String payMethod = "credit";
    String ccName = "qwe qwe";
    String ccNumberErr = "123412341234123"; //15 symbols
    String ccExpiration = "05/2029";
    String ccCVV = "001";
    String responseText;

    //function returns the value of "field" node from "textJSON" JSON string
    String getFieldValue(String textJSON, String field) throws IOException {
        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();
        //read JSON like DOM Parser
        JsonNode rootNode = objectMapper.readTree(textJSON);
        JsonNode fieldNode = rootNode.path(field);

        return fieldNode.asText();
    }

    @Test
    @DisplayName("Check discount calculation")
    void checkDiscountCalc() throws ClientProtocolException, IOException {
        String expectedRes;
        //The value of discount is a lehgth of "Promo code" string
        expectedRes = String.valueOf(promoCode.length());

        //Open the http connection and send POST request
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(props.ASUrlAPI() + "/coupon?coupon=" + promoCode);
            HttpResponse httpResponse = httpClient.execute(request);
            responseText = EntityUtils.toString(httpResponse.getEntity());

            //Check the result in response
            Assert.assertEquals(expectedRes, getFieldValue(responseText, "discount"));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Test
    @DisplayName("Card number length less then 16")
    void checkCCNumberLength() throws ClientProtocolException, IOException {
        String expectedRes = "false"; //Expected "false" cause of 15 digits card number

        //via the http connection
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            //forming POST request
            HttpPost request = new HttpPost(props.ASUrlAPI() + "/checkout");
            //Preparing JSON with parameters
            //Better to get it from DB or from file may be - depends on environment
            StringEntity params = new StringEntity("{\"firstName\":\"" + fName + "\", \"lastName\":\"" + lName + "\", \"paymentMethod\":\"" + payMethod +
                    "\", \"cc-name\":\"" + ccName + "\", \"cc-number\":\"" + ccNumberErr + "\", \"cc-expiration\":\"" + ccExpiration + "\", \"cc-cvv\":\"" +
                    ccCVV + "\"} ");
            params.setContentType("application/json");

            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            //Executing POST request
            HttpResponse httpResponse = httpClient.execute(request);
            //Getting the JSON text from response
            responseText = EntityUtils.toString(httpResponse.getEntity());

            //Comparing the value of "success" field with expected
            Assert.assertEquals(expectedRes, getFieldValue(responseText, "success"));
            //System.out.println("success = "+idNode.asText());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
