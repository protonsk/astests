package ru.proto.clicker.ui;


import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;


@Epic("ASTests")
@Feature("Test task")
@DisplayName("Checkout form")
public class ASTests extends BaseUITest {

    WebDriverWait wait;
    String buttonContinueText = "Continue to checkout";
    String expectedVisible = "true";
    WebElement inputEl;
    WebElement errorTextEl;

    public void start() throws InterruptedException {

// Create object of ChromeOptions Class
        ChromeOptions opt=new ChromeOptions();
// pass the debuggerAddress and pass the port along with host. Since I am running test on local so using localhost
        opt.setExperimentalOption("debuggerAddress","localhost:9222 ");
// pass ChromeOptions object to ChromeDriver constructor
        driver=new ChromeDriver(opt);
        driver.switchTo();
        driver.get(props.ASUrl());
        Thread.sleep(3000);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public By WaitForControl(String pName, String pMethod) {
        wait = new WebDriverWait(driver, 20);
        By targetItem = null;

        if (pMethod.equals("Id")) {
            targetItem = By.id(pName);
        }
        else {
            if (pMethod.equals("xPath")) {
                targetItem = By.xpath(pName);
            }
        }
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(targetItem));
        return targetItem;
    }

    String GetDisplayedProp(String controlId, String fieldXPath) {
        inputEl = driver.findElement(By.id(controlId));

        //Find the error text for firstName input field
        //using xpath to check exact element. They haven't had id and are similar all 6
        errorTextEl = driver.findElement(By.xpath(fieldXPath));

        //Save the visibility of error message
        return String.valueOf(errorTextEl.isDisplayed());
    }
    @Test
    @DisplayName("Check mandatory empty fields")
    void mandatoryFieldsIsNull() throws InterruptedException {
        //driver = new ChromeDriver();
        WebElement buttonContinue;


        String actualRes;

        start();

        //Clicking the Continue button
        //The page can load not fast. Wait till button appear
        By element = WaitForControl("//*[text()='" + buttonContinueText +"']", "xPath");
        buttonContinue = driver.findElement(element);
        //System.out.println("Found a button " + buttonContinue.getText());
        //But there is a rendering delay sometimes
        Thread.sleep(2000);
        buttonContinue.click();

        //Let's check all mandatory empty fields
        //---------1--------
        //Checking the firstName field
        actualRes = GetDisplayedProp("firstName", "/html/body/div/div[2]/div[2]/form/div[1]/div[1]/div");

        //Comparing with expected value
        Assert.assertEquals(expectedVisible, actualRes);

        //we can check the other properties of field if needed
        //may be these checks must be in other test cases
        //inputEl.getCssValue("background-image"));
        //inputEl.getCssValue("border-color"));

        //---------2--------
        //Checking the lastName field
        actualRes = GetDisplayedProp("lastName", "/html/body/div/div[2]/div[2]/form/div[1]/div[2]/div");
        //Comparing with expected value
        Assert.assertEquals(expectedVisible, actualRes);


        //---------3--------
        //Checking the cc-name field
        actualRes = GetDisplayedProp("cc-name", "/html/body/div/div[2]/div[2]/form/div[3]/div[1]/div");
        //Comparing with expected value
        Assert.assertEquals(expectedVisible, actualRes);


        //---------4--------
        //Checking the cc-number field
        actualRes = GetDisplayedProp("cc-number", "/html/body/div/div[2]/div[2]/form/div[3]/div[2]/div");
        //Comparing with expected value
        Assert.assertEquals(expectedVisible, actualRes);


        //---------5--------
        //Checking the cc-expiration field
        actualRes = GetDisplayedProp("cc-expiration", "/html/body/div/div[2]/div[2]/form/div[4]/div[1]/div");
        //Comparing with expected value
        Assert.assertEquals(expectedVisible, actualRes);


        //---------6--------
        //Checking the cc-cvv field
        actualRes = GetDisplayedProp("cc-cvv", "/html/body/div/div[2]/div[2]/form/div[4]/div[2]/div");
        //Comparing with expected value
        Assert.assertEquals(expectedVisible, actualRes);

        driver.quit();
    }


    @Test
    @DisplayName("Check Cart displayed")
    void checkCartAppears() throws InterruptedException {
        WebElement totalField;
        String actualRes;
        //define it here. But we can get it in runtime from database if needed
        String expectedRes = "89";

        start();

        Thread.sleep(2000);
        //Wait for totalAmount value to display
        By element = WaitForControl("totalAmount", "Id");
        totalField = driver.findElement(element);
        //Get the value
        actualRes = totalField.getText();

        //Checking with expected value
        Assert.assertEquals(expectedRes, actualRes);


        driver.quit();
    }

    @Test
    @DisplayName("Check the Promo code")
    void checkPromo() throws InterruptedException {
        WebElement totalField;
        WebElement promoField;
        WebElement redeemButton;
        Float total;
        Float totalAfter;
        String actualRes;
        String promoCode = "12";

        start();
        Thread.sleep(2000);

        //Wait for totalAmount value to display
        By element = WaitForControl("totalAmount", "Id");
        totalField = driver.findElement(element);
        //Get the value before Promo
        total = Float.valueOf(totalField.getText());

        //input the Promo code
        promoField = driver.findElement(By.id("promoCode"));
        promoField.sendKeys(promoCode);

        //Press the Redeem button
        redeemButton = driver.findElement(By.xpath("//*[text()='Redeem']"));
        redeemButton.click();

        //Get the redeemed value
        Thread.sleep(1000);
        actualRes = totalField.getText();
        totalAfter = Float.valueOf(actualRes);

        //Calc the right total value after the Promo code application
        total = total * (100-promoCode.length())/100;

        //System.out.println("Total expected: " + total);

        //Checking with expected value
        //tricky moment with rounding. The rounding method may differ in tested system
        Assert.assertEquals(String.format("%.2f",total), String.format("%.2f",totalAfter));


        driver.quit();
    }

    void fillText(String fieldId, String text) {
        WebElement inputEl;

        inputEl = driver.findElement(By.id(fieldId));
        inputEl.sendKeys(text);
    }

    @Test
    @DisplayName("Order placed")
    void orderPlaced() throws InterruptedException {
        WebElement successField;
        WebElement inputEl;
        WebElement buttonContinue;
        String actualRes;
        //define it here. But we can get it in runtime from database if needed
        String expectedRes = "Your order was placed";
        String fName = "qwe";
        String lName = "qwe";
        String ccName = "qwe qwe";
        String ccNumber = "1234123412341234";
        String ccExpiration = "05/2028";
        String ccCVV = "123";

        start();

        //Filling the
        fillText("firstName", fName);
        fillText("lastName", lName);
        fillText("cc-name", ccName);
        fillText("cc-number", ccNumber);
        fillText("cc-expiration", ccExpiration);
        fillText("cc-cvv", ccCVV);


        By element = WaitForControl("//*[text()='" + buttonContinueText +"']", "xPath");
        buttonContinue = driver.findElement(element);
        //System.out.println("Found a button " + buttonContinue.getText());
        buttonContinue.click();

        //Thread.sleep(1000);
        //Wait for totalAmount value to display
        wait = new WebDriverWait(driver, 20);
        successField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("success")));

        actualRes = String.valueOf(successField.isDisplayed());
        Assert.assertEquals(expectedVisible, actualRes);


        actualRes = successField.getText();
        Assert.assertTrue(actualRes.contains(expectedRes));

        driver.quit();
    }
}
