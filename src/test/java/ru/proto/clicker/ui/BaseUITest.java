package ru.proto.clicker.ui;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.aeonbits.owner.ConfigFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.platform.commons.util.StringUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.proto.clicker.Props;

abstract class BaseUITest {

    private static final Logger LOG = LoggerFactory.getLogger(BaseUITest.class);
    protected static Props props = ConfigFactory.create(Props.class);
    private static boolean initialized = false;
    WebDriver driver;

    @BeforeAll
    private static void setup() {
        if (initialized) return;
        if (!StringUtils.isBlank(props.selenoidUrl())) {
            LOG.info("Tests will be running in Selenoid " + props.selenoidUrl());
            Configuration.remote = props.selenoidUrl();
        } else {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/webdrivers/chromedriver.exe");
        }
        Configuration.baseUrl = props.ASUrl();
        Configuration.startMaximized = true;
        Configuration.timeout = props.timeout();
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(true));
        initialized = true;
    }

    @AfterEach
    void afterTest() {
        Selenide.closeWebDriver();
    }
}
