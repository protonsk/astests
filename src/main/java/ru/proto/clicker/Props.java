package ru.proto.clicker;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "system:properties",
        "system:env",
        "file:src/test/resources/test.properties"
})
public interface Props extends Config {

    @Key("AS.url")
    String ASUrl();

    @Key("AS.urlAPI")
    String ASUrlAPI();

    @Key("selenoid.url")
    String selenoidUrl();

    @Key("timeout")
    @DefaultValue(value = "10000")
    int timeout();
}
